(function(){
	var app = angular.module('flapperNews',['ui.router']);
	
	// app.config([
	// 	'$stateProvider',
	// 	'$urlRouterProvider',
	// 	function($stateProvider, $urlRouterProvider) {

	// 	  	$stateProvider
	// 	    .state('home', {
	// 	      url: '/home',
	// 	      templateUrl: '/home.html',
	// 	      controller: 'MainCtrl'
	// 	    });

	// 	  $urlRouterProvider.otherwise('home');
	// }]);
	app.config(['$stateProvider','$urlRouterProvider',
		function($stateProvider,$urlRouterProvider){
			$stateProvider
			.state('home',{
				url:'/home',
				templateUrl:'/home.html',
				controller:('MainController')
			})
			.state('posts',{
				url:'/posts/{id}',
				templateUrl:'/posts.html',
				controller:'PostController'
			});
			$urlRouterProvider.otherwise('home');
	}]);


	app.factory('posts', [function(){
		var o = {
			posts: []
		};
		return o;
	}]);

	app.controller('MainController',['posts',function(posts){
		this.test = 'Hello world!';
		// this.posts = [
		// 	{title: 'post 1', upvotes: 4},
		// 	{title: 'post 2', upvotes: 2},
		// 	{title: 'post 3', upvotes: 7},
		// 	{title: 'post 4', upvotes: 6},
		// 	{title: 'post 5', upvotes: 1},
		// 	{title: 'post 6', upvotes: 3}
		// ];
		this.posts=posts.posts;
		this.upvote = function(post){
			post.upvotes+=1;
		};

		this.post = {};
		// this.addPost = function(){
		// 	if(!(this.post.title) || this.post.title===''){return;}
		// 	this.posts.push({title: this.post.title,link: this.post.link, upvotes: 0});
		// 	this.post.title = '';
		// 	this.post.link = '';
		// }
		this.addPost = function(){
		  if(!(this.post.title) || this.post.title===''){return;}
		  this.posts.push({
		    title: this.post.title,
		    link: this.post.link,
		    upvotes: 0,
		    comments: [
			    {author: 'Joe', body: 'Cool post!', upvotes: 0},
			    {author: 'Bob', body: 'Great idea but everything is wrong!', upvotes: 0}
			  ]
		  });
		  this.post.title = '';
		  this.post.link = '';
		};

	}]);




})();